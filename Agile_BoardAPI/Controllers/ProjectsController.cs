using System.Collections.Generic;
using System.Threading.Tasks;
using Agile_BoardAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Agile_BoardAPI
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ProjectsController(DatabaseContext context)
        {
            _context = context;
        }

        /*
        GETs
        */
        [HttpGet]
        //https://localhost:5001/projects
        public async Task<ActionResult<IEnumerable<Project>>> getProjects()
        {
            return await _context.projects.ToListAsync();
        }

        //https://localhost:5001/projects/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> getProject(long id)
        {
            var project = await _context.projects.FindAsync(id);
            if (project == null)
            {
                NotFound();
            }
            return project;
        }

        /*
        POSTs
        */
        [HttpPost]
        //https://localhost:5001/projects
        public async Task<ActionResult<Project>> postProject(Project project)
        {
            _context.projects.Add(project);
            await _context.SaveChangesAsync();
            return CreatedAtAction("getProjects", new { id = project.id }, project);
        }

        /*
        PUT
        https://localhost:5001/projects/id
        */
        [HttpPut("{id}")]
        public async Task<ActionResult<Project>> updateProject(long id, Project project)
        {
            if (id != project.id)
            {
                return project;
            }
            _context.Entry(project).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("getProject", new { id = project.id }, project);
        }

        /*
        DELETE
        //https://localhost:5001/projects/id
        */
        [HttpDelete("{id}")]
        public async Task<ActionResult<Project>> deleteProject(long id)
        {
            var project = await _context.projects.FindAsync(id);
            if (project == null)
            {
                return NotFound();
            }
            _context.projects.Remove(project);
            await _context.SaveChangesAsync();
            return project;
        }
    }
}