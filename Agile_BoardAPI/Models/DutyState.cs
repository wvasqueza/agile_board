namespace Agile_BoardAPI
{
    public class DutyState
    {
        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public long dutyID { get; set; }

    }
}